package com.app.movieExplorer.states

import com.app.movieExplorer.api.model.data.OmdbSearchData

sealed class HomeState {
    object Loading : HomeState()
    data class ResultSearch(val data: OmdbSearchData) : HomeState()
    data class Exception(val callErrors: Error) : HomeState()
}
