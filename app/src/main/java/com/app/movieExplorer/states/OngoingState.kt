package com.app.movieExplorer.states

import com.app.movieExplorer.structs.MovieDetailsItem
import kotlinx.coroutines.CoroutineExceptionHandler

sealed class OngoingState {
    object Loading : OngoingState()
    data class Detail(var data: MovieDetailsItem) : OngoingState()
    data class Exception(var callErrors: CoroutineExceptionHandler) : OngoingState()
}
