package com.app.movieExplorer.viewModels

import android.content.SharedPreferences
import android.content.res.Resources
import com.app.movieExplorer.api.model.OmdbApi
import com.app.movieExplorer.api.model.OmdbClient
import com.app.movieExplorer.di.GlideApp
import com.app.movieExplorer.di.get
import com.app.movieExplorer.repositories.MovieRepository
import com.app.movieExplorer.ui.templates.ScopeViewModel
import com.app.movieExplorer.utils.providers.ProvideLiveData
import com.app.movieExplorer.utils.CacheHandler
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.system.measureTimeMillis

class SplashViewModel : ScopeViewModel() {
    val eventInitialized = ProvideLiveData<Unit>()

    fun initializeAll() = scope.launch {
        val time = measureTimeMillis {
            withContext(IO) {
                // Initialize lazy tasks.
                initializeDi()
                initializeGlide()
            }
        }

        delay(MIN_DELAY_MILLISEC - time)

        eventInitialized.triggerEvent(Unit)
    }

    private fun initializeDi() {
        get<Resources>()
        get<SharedPreferences>()

        get<OmdbApi>()
        get<OmdbClient>()

        get<CacheHandler>()
        get<MovieRepository>()
    }

    private fun initializeGlide() {
        GlideApp.get(get())
    }

    companion object {
        private const val MIN_DELAY_MILLISEC = 1_000
    }
}