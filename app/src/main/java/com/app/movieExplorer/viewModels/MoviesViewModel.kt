package com.app.movieExplorer.viewModels

import com.app.movieExplorer.di.get
import com.app.movieExplorer.repositories.MovieRepository
import com.app.movieExplorer.structs.MovieItem
import com.app.movieExplorer.ui.templates.ScopeViewModel
import com.app.movieExplorer.utils.providers.ProvideLiveData
import com.app.movieExplorer.utils.providers.ProvideObserver
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class MoviesViewModel : ScopeViewModel() {
    val movies = ProvideObserver(listOf<MovieItem>(), isTriggerNotEquals = false)

    val loading = ProvideObserver(false)

    val eventError = ProvideLiveData<Throwable>()
    val eventClicked = ProvideLiveData<MovieItem>()

    private var lastQuery = "Movie"
    private var offset: Int = 0
    private var page: Int = 1

    var isLimitReached: Boolean = false

    fun initialize() {
        search(lastQuery)
    }

    fun search(query: String) {
        loading.value = false
        movies.value = listOf()

        offset = 0
        page = 1
        isLimitReached = false
        lastQuery = query

        loadMovies()
    }

    fun loadMovies() {
        if (isLimitReached) return

        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            Timber.w(throwable)
            loading.value = false
            eventError.triggerEvent(throwable)
        }

        scope.launch(exceptionHandler) {
            loading.value = true

            val (items, total) =  withContext(IO) {
                get<MovieRepository>().search(lastQuery, page)
            }

            isLimitReached = offset + items.size >= total
            offset += items.size
            page++

            movies.value = movies.value.toMutableList().apply {
                addAll(items)
            }

            loading.value = false
        }
    }

    fun onMovieClick(item: MovieItem) {
        eventClicked.triggerEvent(item)
    }
}