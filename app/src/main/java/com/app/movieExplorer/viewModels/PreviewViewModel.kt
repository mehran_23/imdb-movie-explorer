package com.app.movieExplorer.viewModels

import com.app.movieExplorer.ui.templates.BaseViewModel
import com.app.movieExplorer.utils.providers.ProvideNullableObserver

class PreviewViewModel : BaseViewModel() {
    var imageUri = ProvideNullableObserver<String>(null)
}