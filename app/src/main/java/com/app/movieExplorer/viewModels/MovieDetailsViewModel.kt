package com.app.movieExplorer.viewModels

import com.app.movieExplorer.di.get
import com.app.movieExplorer.repositories.MovieRepository
import com.app.movieExplorer.states.OngoingState
import com.app.movieExplorer.structs.MovieDetailsItem
import com.app.movieExplorer.ui.templates.ScopeViewModel
import com.app.movieExplorer.utils.providers.ProvideLiveData
import com.app.movieExplorer.utils.providers.ProvideObserver
import com.app.movieExplorer.utils.providers.ProvideNullableObserver
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class MovieDetailsViewModel : ScopeViewModel() {
    val details = ProvideNullableObserver<MovieDetailsItem?>(null)

    val loading = ProvideObserver(false)

    val eventError = ProvideLiveData<Throwable>()

    lateinit var imdbId: String

    fun loadDetails() {

        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            Timber.w(throwable)
            loading.value = false
            eventError.triggerEvent(throwable)
        }

        scope.launch(exceptionHandler) {
//            save states
            loading.value = true

            val result = withContext(IO) {
                get<MovieRepository>().details(imdbId)
            }

            //reduce handler

            details.value = result
            loading.value = false

        }
    }
}