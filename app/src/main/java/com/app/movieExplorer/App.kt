package com.app.movieExplorer

import androidx.multidex.MultiDexApplication
import com.app.movieExplorer.BuildConfig.DEBUG
import com.app.movieExplorer.di.contextModule
import com.app.movieExplorer.di.networkModule
import com.app.movieExplorer.di.storageModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.logger.AndroidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.EmptyLogger
import timber.log.Timber

class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()

        // Initialize Koin DI modules.

        startKoin {
            androidContext(this@App)
            modules(listOf(contextModule, networkModule, storageModule))
            logger(if (DEBUG) AndroidLogger() else EmptyLogger())
        }

        // Initialize logger.

        if (DEBUG) Timber.plant(Timber.DebugTree())
    }
}