package com.app.movieExplorer.api.model.interceptor

class responseHandler<T : Any>(
    val data: T? = null,
    val throwable: Throwable? = null
) {
    fun getDataOrThrow(): T =
        data ?: throw (throwable ?: IllegalStateException("Unknown exception."))
}