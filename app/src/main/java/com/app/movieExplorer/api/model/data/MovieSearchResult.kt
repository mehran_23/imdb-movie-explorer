package com.app.movieExplorer.api.model.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MovieSearchResult(
    @Expose
    @SerializedName("Search")
    val search: List<MovieSearchSubject>? = null,

    @Expose
    @SerializedName("totalResults")
    val total: String? = null
) : MovieResult()