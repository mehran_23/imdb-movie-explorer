package com.app.movieExplorer.api.model.data

import com.app.movieExplorer.structs.MovieItem

typealias OmdbSearchData = Pair<List<MovieItem>, Int>