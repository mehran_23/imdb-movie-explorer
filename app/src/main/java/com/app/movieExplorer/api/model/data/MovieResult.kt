package com.app.movieExplorer.api.model.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

abstract class MovieResult(
    @Expose
    @SerializedName("Response")
    val response: String? = null,

    @Expose
    @SerializedName("Error")
    val error: String? = null
)