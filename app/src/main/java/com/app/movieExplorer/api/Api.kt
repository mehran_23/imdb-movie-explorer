package com.app.movieExplorer.api.model

import com.app.movieExplorer.api.model.data.OmdbSearchData
import com.app.movieExplorer.api.model.data.DetailsResult
import com.app.movieExplorer.api.model.data.MovieResult
import com.app.movieExplorer.api.model.data.MovieSearchResult
import com.app.movieExplorer.setting.NetworkConfig
import com.app.movieExplorer.di.get
import com.app.movieExplorer.structs.MovieDetailsItem
import com.app.movieExplorer.structs.MovieItem
import retrofit2.http.GET
import retrofit2.http.Query

interface OmdbApi {
    @GET(".")
    suspend fun search(
        @Query("apikey") key: String,
        @Query("s") query: String,
        @Query("page") page: Int
    ): MovieSearchResult

    @GET(".")
    suspend fun details(
        @Query("apikey") key: String,
        @Query("i") imdbId: String,
        @Query("plot") plot: String
    ): DetailsResult
}

class OmdbClient {
    suspend fun search(
        query: String,
        page: Int
    ): OmdbSearchData {
        val key = randomKey()
        val result = get<OmdbApi>().search(key, query, page).also {
            checkResult(it)
        }

        val total = result.total?.toIntOrNull() ?: throw RuntimeException("Wrong total.")
        val movies = result.search?.map { MovieItem.fromSubjectOrThrow(it) } ?: listOf()
        return movies to total
    }

    suspend fun details(
        imdbId: String
    ): MovieDetailsItem {
        val key = randomKey()
        val plot = "full"
        val result = get<OmdbApi>().details(key, imdbId, plot).also {
            checkResult(it)
        }

        return MovieDetailsItem.fromResultOrThrow(result)
    }

    companion object {
        fun randomKey() = get<NetworkConfig>().omdbKeys.random()

        fun checkResult(result: MovieResult) = with(result) {
            if (response != "True") throw RuntimeException(error ?: "Unknown error!")
        }
    }
}