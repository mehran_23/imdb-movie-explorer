package com.app.movieExplorer.ui.fragments

import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import com.app.movieExplorer.R
import com.app.movieExplorer.databinding.FragmentSplashBinding
import com.app.movieExplorer.ui.fragments.SplashFragmentDirections.Companion.actionSplashToMain
import com.app.movieExplorer.ui.templates.MvvmFragment
import com.app.movieExplorer.viewModels.SplashViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_splash.*

class SplashFragment : MvvmFragment<FragmentSplashBinding, SplashViewModel>(
    R.layout.fragment_splash,
    SplashViewModel::class.java
) {
    private val startDelay=6000L

    override fun onEveryInitialization(savedBundle: Bundle?) {

        loadGif(R.drawable.gif_splash)

        lazyStartActivity(startDelay);
    }

    override fun onFirstInitialization() {
        data.initializeAll()
    }

    fun loadGif(gifId: Int) {
        Glide.with(this)
            .load(gifId)
            .into(img_logo)
    }

    fun lazyStartActivity(delay: Long) {

        Handler().postDelayed(
            {
                //                add noHistory property in manifest for finish activity

                data.eventInitialized.observeEvent(viewLifecycleOwner, {
                    applicationNavigation?.navigate(actionSplashToMain())
                })
            }, delay
        )
    }
}
