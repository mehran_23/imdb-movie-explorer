package com.app.movieExplorer.ui.fragments

import android.os.Bundle

import com.app.movieExplorer.R
import com.app.movieExplorer.databinding.FragmentBottomBinding
import com.app.movieExplorer.ui.templates.MvvmFragment
import com.app.movieExplorer.viewModels.BottomViewModel

class BottomFragment : MvvmFragment<FragmentBottomBinding, BottomViewModel>(
    R.layout.fragment_bottom,
    BottomViewModel::class.java
) {
    override fun onEveryInitialization(savedBundle: Bundle?) {
        actionBar?.apply {
            setDisplayShowTitleEnabled(true)
            setDisplayHomeAsUpEnabled(false)
            setHomeButtonEnabled(false)
        }
    }

    override fun onBackPressed(): Boolean = when (mainNavigation?.currentDestination?.id) {
        R.id.destinationMovies -> {
            activity?.finish()
            true
        }

        else -> {
            true
        }
    }
}