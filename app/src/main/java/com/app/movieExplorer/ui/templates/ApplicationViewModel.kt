package com.app.movieExplorer.ui.templates

import com.app.movieExplorer.di.get
import kotlinx.coroutines.*

class ApplicationViewModel : ScopeViewModel() {
    override fun onCleared() {
        // Finish all tasks on application finished.
        get<Job>().cancelChildren()
    }
}