package com.app.movieExplorer.ui.fragments

import androidx.navigation.fragment.navArgs
import com.app.movieExplorer.R
import com.app.movieExplorer.databinding.FragmentPreviewBinding
import com.app.movieExplorer.ui.templates.MvvmFragment
import com.app.movieExplorer.viewModels.PreviewViewModel

class PreviewFragment : MvvmFragment<FragmentPreviewBinding, PreviewViewModel>(
    R.layout.fragment_preview,
    PreviewViewModel::class.java
) {
    private val args by navArgs<PreviewFragmentArgs>()

    override fun onFirstInitialization() {
        data.imageUri.value = args.imageUri
    }
}