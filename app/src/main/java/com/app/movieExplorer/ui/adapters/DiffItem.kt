package com.app.movieExplorer.ui.adapters

interface DiffItem {
    fun id(): Any? = hashCode()
}