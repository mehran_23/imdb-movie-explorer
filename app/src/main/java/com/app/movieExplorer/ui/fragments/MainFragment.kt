package com.app.movieExplorer.ui.fragments

import android.os.Bundle
import com.app.movieExplorer.R
import com.app.movieExplorer.databinding.FragmentMainBinding
import com.app.movieExplorer.ui.templates.MvvmFragment
import com.app.movieExplorer.viewModels.MainViewModel
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : MvvmFragment<FragmentMainBinding, MainViewModel>(
    R.layout.fragment_main,
    MainViewModel::class.java
) {
    override fun onEveryInitialization(savedBundle: Bundle?) {
        activity?.setSupportActionBar(mainToolbar)
    }
}