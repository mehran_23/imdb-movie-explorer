package com.app.movieExplorer.repositories

import com.app.movieExplorer.api.model.OmdbClient
import com.app.movieExplorer.di.get
import com.app.movieExplorer.utils.CacheHandler
import com.app.movieExplorer.utils.CacheHandler.Companion.cacheKey
import java.util.concurrent.TimeUnit

class MovieRepository {
    suspend fun search(
        query: String,
        page: Int = 1
    ) = get<CacheHandler>().run {
        val key = cacheKey("${OMDB_PREFIX}search", query, page)
        getOrPut(key, suspend {
            get<OmdbClient>().search(query, page)
        }, TimeUnit.MINUTES.toMillis(3))
    }

    suspend fun details(
        imdbId: String
    ) = get<CacheHandler>().run {
        val key = cacheKey("${OMDB_PREFIX}details", imdbId)
        getOrPut(key, suspend {
            get<OmdbClient>().details(imdbId)
        }, TimeUnit.MINUTES.toMillis(2))
    }

    companion object {
        private const val OMDB_PREFIX = "omdb"
    }
}