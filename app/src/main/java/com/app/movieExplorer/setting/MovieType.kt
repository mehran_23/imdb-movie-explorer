package com.app.movieExplorer.setting

enum class MovieType { MOVIE, SERIES }