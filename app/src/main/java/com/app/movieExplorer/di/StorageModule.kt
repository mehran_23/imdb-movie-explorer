package com.app.movieExplorer.di

import com.app.movieExplorer.BuildConfig.*
import com.app.movieExplorer.setting.CacheConfig
import com.app.movieExplorer.repositories.MovieRepository
import com.app.movieExplorer.utils.CacheHandler
import com.jakewharton.disklrucache.DiskLruCache
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import java.io.File

val storageModule = module {
    single { CacheConfig() }

    single {
        val config = get<CacheConfig>()

        val cacheDir = File(androidContext().cacheDir, "timed")

        val cache = DiskLruCache.open(
            cacheDir, VERSION_CODE,
            2, config.commonCacheSizeBytes
        )

        CacheHandler(cache)
    }

    single { MovieRepository() }
}